require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "follow_user" do
    user1 = User.new({name: 'guillordin', firstname: 'Gerald'})
    user2 = User.new({name: 'guillordine', firstname: 'Geraldine'})

    assert user1.save
    assert user2.save

    user1.follow user2
    assert user1.follow? user2
  end

  test "unfollow_user" do
    user1 = User.new({name: 'guillordin', firstname: 'Gerald'})
    user2 = User.new({name: 'guillordine', firstname: 'Geraldine'})
    assert user1.save
    assert user2.save

    user1.follow user2
    assert user1.follow? user2

    user1.unfollow(user2)
    assert_not user1.follow? user2
  end

  test "follow?_method" do
    user1 = User.new({name: 'guillordin', firstname: 'Gerald'})
    user2 = User.new({name: 'guillordine', firstname: 'Geraldine'})

    assert user1.save
    assert user2.save

    assert_not user1.follow? user2

    user1.follow user2
    assert user1.follow? user2
  end

  test "can write_tweet" do
    tweet = Tweet.new({content: 'Tweet a tester', id: '1'})

    assert tweet.save
  end

  test "user_can_have_many_tweets" do
    user = User.new({name: 'guillordin', firstname: 'Gerald'})

    assert user.save

    assert user.tweets.create({content: 'Tweet a tester', user_id: '1'})
    assert user.tweets.create({content: 'Tweet2 a tester', user_id: '1'})
    assert user.tweets.create({content: 'Tweet3 a tester', user_id: '1'})

    assert_not_empty user.tweets
  end

  test "user_can_have_followers" do
    user = User.new({name: 'guillordin', firstname: 'Gerald'})
    follower = User.new({name: 'guillordine', firstname: 'Geraldine'})

    assert user.save
    assert follower.save
    
    assert_empty user.followers

    follower.follow user

    assert_not_empty user.followers
  end

  test "user_can_follow" do
    user = User.new({name: 'guillordin', firstname: 'Gerald'})
    following = User.new({name: 'guillordine', firstname: 'Geraldine'})

    assert user.save
    assert following.save

    assert_empty user.following

    user.follow following

    assert_not_empty user.following
  end

end
end
