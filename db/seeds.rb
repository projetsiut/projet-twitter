# ruby encoding: utf-8
#
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Seed users
marmo = User.create({name: 'Marmorat', firstname: 'Guillaume', img_url: ''})
chall = User.create({name: 'Challut', firstname: 'Clement', img_url: ''})
serra = User.create({name: 'Serraz', firstname: 'Mathieu', img_url: ''})
# Seed some tweets
marmo.tweets.create({content: 'C\'est le premier tweet'})
marmo.tweets.create({content: 'Je retweet'})
marmo.tweets.create({content: 'Blaah #johndoe'})
chall.tweets.create({content: 'Moi aussi je tweet mais je galère'})
chall.tweets.create({content: 'Parce que le ruby c chaud #peon'})
serra.tweets.create({content: 'Salut, moi c serra! il est bien ce fake tweeter #renard #johndoe'})
# Seed relation between users
marmo.follow chall
chall.follow marmo
chall.follow serra
serra.follow marmo
# Add hashtag in db because of bahavior is running in controller instead of model for parsing hashtag
Hashtag.create({hashtag: '#johndoe', tweet_id: 3})
Hashtag.create({hashtag: '#johndoe', tweet_id: 6})
Hashtag.create({hashtag: '#peon', tweet_id: 5})
Hashtag.create({hashtag: '#renard', tweet_id: 6})