class CreateHashtags < ActiveRecord::Migration
  def change
    create_table :hashtags do |t|
      t.string :hashtag
      t.integer :tweet_id

      t.timestamps null: false
    end
  end
end
