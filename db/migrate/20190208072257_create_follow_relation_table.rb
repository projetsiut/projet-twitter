class CreateFollowRelationTable < ActiveRecord::Migration
  def change
    create_table :follow_relations do |t|
      t.integer :follower_id
      t.integer :followed_id

      t.timestamps null: false
    end
    add_index :follow_relations, :follower_id
    add_index :follow_relations, :followed_id
    add_index :follow_relations, [:follower_id, :followed_id], unique: true
  end

end
