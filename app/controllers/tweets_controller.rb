class TweetsController < ApplicationController

  before_action :find_user

  def index
    @users = User.all
    # @tweets = @user.tweets.order 'tweets.created_at DESC'

    ids = @user.following.pluck(:id)
    ids << @user.id
    @tweets = Tweet.where(user_id: ids).order 'tweets.created_at DESC'

    # OLD -- sort not work ?
    # @hashtags = Hashtag.group(:hashtag).limit(10).count
    # @hashtags.sort_by {|e| -e[1]}
    # New with sort in sql
    sql = "SELECT hashtag, count(hashtag) FROM hashtags GROUP BY hashtag ORDER BY 2 DESC"
    @hashtags = ActiveRecord::Base.connection.execute(sql)
  end

  def show

  end

  def new
    @tweet = Tweet.new
  end

  def create
    tweet = @user.tweets.create(tweet_params)

    respond_to do |format|
      if @user.save
        process_hashtags(tweet)

        format.html { redirect_to user_tweets_path(@user), notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  private

    def tweet_params
      params.require(:tweet).permit(:user_id, :content)
    end

    def find_user
      @user = User.find_by_id(params[:user_id])
    end

    def process_hashtags(tweet)
      tweet.content.scan(/\B#\w*[a-zA-Z]+\w*/).each do |tag|
        h = Hashtag.create({hashtag: tag, tweet_id: tweet.id })
        h.save
      end
    end

end
