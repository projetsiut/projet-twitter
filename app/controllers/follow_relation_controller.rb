class FollowRelationController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def create
    user = User.find_by(id: params[:user_id])
    to_follow = User.find_by(id: params[:follower_id])

    respond_to do |format|
      if user.follow(to_follow)
        format.html { redirect_to user_tweets_path(user_id: params[:user_id]), notice: 'User successfully followed.' }
      else
        format.html { redirect_to user_tweets_path(user_id: params[:user_id]), notice: 'Error while creating relation' }
      end
    end
  end

  def destroy
    user = User.find_by(id: params[:user_id])
    to_unfollow = User.find_by(id: params[:follower_id])

    respond_to do |format|
      if user.unfollow(to_unfollow)
        format.html { redirect_to user_tweets_path(user_id: params[:user_id]), notice: 'User successfully unfollow.' }
      else
        format.html { redirect_to user_tweets_path(user_id: params[:user_id]), notice: 'Error while creating relation' }
      end
    end
  end

end
