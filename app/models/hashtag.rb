class Hashtag < ActiveRecord::Base
  has_many :tweets

  validates :tweet_id, presence: true
end
