class User < ActiveRecord::Base
  has_many :tweets

  has_many :active_relationships, class_name: "FollowRelation", foreign_key: "follower_id", dependent: :destroy
  has_many :passive_relationships, class_name: "FollowRelation", foreign_key: "followed_id", dependent: :destroy

  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  def follow(followed)
    active_relationships.create(followed_id: followed.id)
  end

  def unfollow(followed)
    active_relationships.where(followed_id: followed.id).first.destroy
  end

  def follow?(user)
    following.include?(user)
  end
end
