class Tweet < ActiveRecord::Base
  belongs_to :user
  has_many :hashtags

  validates :content, presence: true
  validates :content, length: { in: 1..140 }
end
