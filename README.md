**Procedure d'installation projet-twitter**
--

Realisation : 

- Guillaume Marmorat
- Clement Challut

---
        # -- On clone le dépot
        git clone https://gitlab.com/guillaumeMarmorat/projet-twitter.git
        cd ./projet-twitter
        
        # -- Téléchargement des module rails et des dependances
        bundle install --path vendor/bundle
        
        # -- Mise en place de la base de donnée
        rake db:setup
        
        # -- Demarrage du serveur rails
        rails s
---

Le projet est pret à être utilisé.. Enjoy!
        